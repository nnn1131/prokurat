import { TextInput, Checkbox, Button, Group, Box } from "@mantine/core";
import { DatePicker, TimeInput } from "@mantine/dates";
import { useForm } from "@mantine/form";
import React, { useState } from "react";

interface NewRecordProps {
  onSubmit: (values) => any;
}

export function NewRecord({ onSubmit }: NewRecordProps) {
  const form = useForm({
    initialValues: {
      email: "",
      start: "",
      end: "",
      name: "",
      description: "",
    },

    validate: {
      email: (value) => (/^\S+@\S+$/.test(value) ? null : "Invalid email"),
    },
  });

  const [startDate, setStartDate] = useState(new Date(Date.now()));
  const [endDate, setEndDate] = useState(new Date(Date.now()));

  // TODO: set initial value
  //   form.setFieldValue(
  //     "start",
  //     startDate.toLocaleString("en-US", {
  //       weekday: "short", // long, short, narrow
  //       day: "numeric", // numeric, 2-digit
  //       year: "numeric", // numeric, 2-digit
  //       month: "long", // numeric, 2-digit, long, short, narrow
  //       hour: "numeric", // numeric, 2-digit
  //       minute: "numeric", // numeric, 2-digit
  //       second: "numeric", // numeric, 2-digit
  //     })
  //   );
  //   form.setFieldValue(
  //     "end",
  //     endDate.toLocaleString("en-US", {
  //       weekday: "short", // long, short, narrow
  //       day: "numeric", // numeric, 2-digit
  //       year: "numeric", // numeric, 2-digit
  //       month: "long", // numeric, 2-digit, long, short, narrow
  //       hour: "numeric", // numeric, 2-digit
  //       minute: "numeric", // numeric, 2-digit
  //       second: "numeric", // numeric, 2-digit
  //     })
  //   );

  return (
    <Box sx={{ maxWidth: 300 }} mx="auto" style={{ flexDirection: "row" }}>
      <form
        onSubmit={form.onSubmit((values) => onSubmit({ user: {}, ...values }))}
      >
        <TextInput
          required
          label="Email"
          placeholder="your@email.com"
          {...form.getInputProps("email")}
        />
        <TextInput
          required
          label="Name"
          placeholder="Task name"
          {...form.getInputProps("name")}
        />
        <TextInput
          label="Description"
          placeholder="Task description"
          {...form.getInputProps("description")}
        />

        <Group style={{ display: "flex", alignItems: "flex-end" }}>
          <DatePicker
            required
            label={"Set start day & time"}
            value={startDate}
            onChange={(value) => {
              const a = new Date();
              a.setFullYear(value.getFullYear());
              a.setMonth(value.getMonth());
              a.setDate(value.getDate());
              a.setHours(startDate.getHours());
              a.setMinutes(startDate.getMinutes());

              setStartDate(a);
              form.setFieldValue(
                "start",
                a.toLocaleString("en-US", {
                  weekday: "short", // long, short, narrow
                  day: "numeric", // numeric, 2-digit
                  year: "numeric", // numeric, 2-digit
                  month: "long", // numeric, 2-digit, long, short, narrow
                  hour: "numeric", // numeric, 2-digit
                  minute: "numeric", // numeric, 2-digit
                  second: "numeric", // numeric, 2-digit
                })
              );
            }}
          />
          <TimeInput
            value={startDate}
            onChange={(value) => {
              const a = new Date(startDate);
              a.setHours(value.getHours());
              a.setMinutes(value.getMinutes());

              setStartDate(a);
              form.setFieldValue(
                "start",
                a.toLocaleString("en-US", {
                  weekday: "short", // long, short, narrow
                  day: "numeric", // numeric, 2-digit
                  year: "numeric", // numeric, 2-digit
                  month: "long", // numeric, 2-digit, long, short, narrow
                  hour: "numeric", // numeric, 2-digit
                  minute: "numeric", // numeric, 2-digit
                  second: "numeric", // numeric, 2-digit
                })
              );
            }}
          />
        </Group>

        <Group style={{ display: "flex", alignItems: "flex-end" }}>
          <DatePicker
            required
            label={"Set final day & time"}
            value={endDate}
            onChange={(value) => {
              setEndDate(value);
              form.setFieldValue("end", value.toDateString());
            }}
          />
          <TimeInput value={endDate} />
        </Group>

        <Group position="right" mt="md">
          <Button type="submit">Submit</Button>
        </Group>
      </form>
    </Box>
  );
}
