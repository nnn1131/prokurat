import React from "react";
import { Avatar, Table, Group, Text, Anchor, ScrollArea } from "@mantine/core";

export interface User {
  _id: string;
  name: string;
  email: string;
  avatar: string;
}

interface TimestampsTableProps {
  data: {
    _id: string;
    user: User;
    start: string;
    end: string;
    name: string;
    description: string;
  }[];
}

export function TimestampsTable({ data }: TimestampsTableProps) {
  const rows = data.map((item, index) => (
    <tr key={item._id}>
      <td>
        <Text size="sm" weight={500}>
          {index + 1}
        </Text>
      </td>

      <td>
        <Group spacing="sm">
          <Avatar size={30} src={item.user.avatar} radius={30} />
          <Text size="sm" weight={500}>
            {item.user.name}
          </Text>
        </Group>
      </td>

      <td>
        <Anchor<"a">
          size="sm"
          href="#"
          onClick={(event) => event.preventDefault()}
        >
          {item.user.email}
        </Anchor>
      </td>
      <td>
        <Text size="sm" color="gray">
          {item.name}
        </Text>
      </td>
      <td>
        <Group spacing={0} position="apart">
          <Text size="sm" color="gray">
            {item.start}
          </Text>
          <Text size="sm" color="gray">
            {item.end}
          </Text>
        </Group>
      </td>

      <td>
        <Text size="sm" color="gray">
          {item.description}
        </Text>
      </td>
    </tr>
  ));

  return (
    <ScrollArea>
      <Table sx={{ minWidth: 800 }} verticalSpacing="sm">
        <thead>
          <tr>
            <th>#</th>
            <th>Assignee Name</th>
            <th>Email</th>
            <th>Timestamp Name</th>
            <th>Timestamp</th>
            <th>Description</th>
            <th />
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </Table>
    </ScrollArea>
  );
}
