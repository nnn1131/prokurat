import { Button } from "@mantine/core";
import React, { useState } from "react";
import { NewRecord } from "../components/NewRecord";
import { TimestampsTable } from "../components/TimestampsTable";
// import "../styles/globals.css";
import MainLayout from "../layouts/MainLayout";

const users = [
  {
    _id: "6a7d76a8sdyufa76f68as",
    name: "Ivan Kovalev",
    email: "ikaskdooasd@gmail.com",
    avatar: "https://placeholder.pics/svg/300/FF1808/Ivan",
  },
  {
    _id: "6a7d76a8sdtufa76f68as",
    name: "Anton Tumaev",
    email: "ant123@gmail.com",
    avatar: "https://placeholder.pics/svg/300/88FF4C/Anton",
  },
  {
    _id: "6a7d21a8sdtufa76f68as",
    name: "Gleb Grach",
    email: "grach666@gmail.com",
    avatar: "https://placeholder.pics/svg/300/E600FF/Gleb",
  },
];

const mock = [
  {
    _id: "6a7d21a8sdtuasd6f68as",
    user: users[0],
    start: "111",
    end: "222",
    name: "aaa",
    description: "hi, mom!",
  },
  {
    _id: "6a7d21aadghgsd6f68as",
    user: users[2],
    start: "111",
    end: "222",
    name: "aaa",
    description: "hi, mom!",
  },
  {
    _id: "6a7d23333tuasd6f68as",
    user: users[1],
    start: "111",
    end: "222",
    name: "aaa",
    description: "hi, mom!",
  },
  {
    _id: "6768afasftuasd6f68as",
    user: users[2],
    start: "111",
    end: "222",
    name: "aaa",
    description: "hi, mom!",
  },
  {
    _id: "6a7d21a8sdtaaaaf68as",
    user: users[0],
    start: "111",
    end: "222",
    name: "aaa",
    description: "hi, mom!",
  },
  {
    _id: "6a7d21a8sdddsd6f68as",
    user: users[1],
    start: "111",
    end: "222",
    name: "aaa",
    description: "hi, mom!",
  },
];

const Index = () => {
  const [data, setData] = useState(mock);
  const [show, setShow] = useState(false);

  return (
    <>
      <MainLayout>
        <Button onClick={() => setShow(!show)}>Добавить время</Button>
        {show ? (
          <NewRecord
            onSubmit={(values) => {
              console.log(values);
              setData([values, ...data]);
            }}
          />
        ) : null}
        <TimestampsTable data={data}></TimestampsTable>
      </MainLayout>
    </>
  );
};

export default Index;
