import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ServeStaticModule } from '@nestjs/serve-static';
import { resolve } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FileModule } from './files/file.module';
import { TimestampModule } from './timestamps/timestamp.module';
import { UserModule } from './users/user.module';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: resolve(__dirname, 'static'),
    }),
    MongooseModule.forRoot(
      'mongodb+srv://atlas_admin:R8aXGahLHZHC2saS@cluster0.nerhc.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
    ),
    UserModule,
    TimestampModule,
    FileModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
