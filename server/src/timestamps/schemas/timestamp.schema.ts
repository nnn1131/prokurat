import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TimestampDocument = Timestamp & Document;

@Schema()
export class Timestamp {
  @Prop()
  start: string;

  @Prop()
  end: string;

  @Prop()
  name: string;

  @Prop()
  description: string;
}

export const TimestampSchema = SchemaFactory.createForClass(Timestamp);