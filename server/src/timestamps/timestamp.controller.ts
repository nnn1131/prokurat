import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { CreateTimestampDto } from './dto/create-timestamp.dto';
import { Timestamp } from './schemas/timestamp.schema';
import { TimestampService } from './timestamp.service';
import { ObjectId } from 'mongoose';

@Controller('/timestamps')
export class TimestampController {
  constructor(private readonly timestampService: TimestampService) {}

  @Post()
  create(@Body() dto: CreateTimestampDto) {
    return this.timestampService.create(dto);
  }

  @Get()
  async getAll(): Promise<Timestamp[]> {
    return this.timestampService.getAll();
  }

  @Get('/search')
  async search(@Query('query') query: any) {
    return this.timestampService.search(query);
  }

  @Get(':id')
  async getOne(@Param('id') id: ObjectId) {
    return this.timestampService.getOne(id);
  }

  @Delete(':id')
  async delete(@Param('id') id: ObjectId) {
    return this.timestampService.delete(id);
  }
}
