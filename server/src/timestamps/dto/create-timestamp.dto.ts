export class CreateTimestampDto {
    readonly name: string;
    readonly start: string;
    readonly end: string;
    readonly description: string;
}