import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Timestamp, TimestampSchema } from './schemas/timestamp.schema';
import { TimestampController } from './timestamp.controller';
import { TimestampService } from './timestamp.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Timestamp.name, schema: TimestampSchema },
    ]),
  ],
  controllers: [TimestampController],
  providers: [TimestampService],
})
export class TimestampModule {}
