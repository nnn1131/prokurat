import { Model, ObjectId } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Timestamp, TimestampDocument } from './schemas/timestamp.schema';
import { CreateTimestampDto } from './dto/create-timestamp.dto';

@Injectable()
export class TimestampService {
  /**
   *
   */
  constructor(
    @InjectModel(Timestamp.name)
    private timestampModel: Model<TimestampDocument>,
  ) {}

  async create(dto: CreateTimestampDto): Promise<Timestamp> {
    const newts = new this.timestampModel({ ...dto });
    return newts.save();
  }

  async getAll(): Promise<Timestamp[]> {
    const data = await this.timestampModel.find();
    return data;
  }

  async search(query: string): Promise<Timestamp[]> {
    const result = await this.timestampModel.find({
      name: { $regex: new RegExp(query, 'i') },
    });
    return result;
  }

  async getOne(id: ObjectId): Promise<Timestamp> {
    return await this.timestampModel.findById(id);
  }

  async delete(id: ObjectId): Promise<ObjectId> {
    const timestamp = await this.timestampModel.findByIdAndDelete(id);
    return timestamp._id;
  }
}
