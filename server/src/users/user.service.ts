import { Model, ObjectId } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './schemas/user.schema';
import { CreateUserDto } from './dto/create-user.dto';
import { FileService, FileType } from 'src/files/file.service';

@Injectable()
export class UserService {
  /**
   *
   */
  constructor(
    @InjectModel(User.name)
    private userModel: Model<UserDocument>,
    private fileService: FileService,
  ) {}

  async create(dto: CreateUserDto, avatar: Express.Multer.File): Promise<User> {
    const avatarPath = this.fileService.createFile(FileType.IMAGE, avatar);
    const user = new this.userModel({ ...dto, avatar: avatarPath });
    return user.save();
  }

  async getAll(): Promise<User[]> {
    const data = await this.userModel.find();
    return data;
  }

  async getOne(id: ObjectId): Promise<User> {
    return await this.userModel.findById(id);
  }

  async delete(id: ObjectId): Promise<ObjectId> {
    const user = await this.userModel.findByIdAndDelete(id);
    return user._id;
  }
}
